import { TestBed, inject } from '@angular/core/testing';
import { TiposPagamentoService } from './tipos-pagamento.service';

describe('TiposPagamentoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TiposPagamentoService]
    });
  });

  it('should be created', inject([TiposPagamentoService], (service: TiposPagamentoService) => {
    expect(service).toBeTruthy();
  }));
});
