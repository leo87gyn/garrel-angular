import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class CategoriasService {
  private url = environment.url+ "categorias";
  private urlVerificarNomeCategoria = environment.url+ "verificarCategoria";

 
  //private url_excluir = environment.url+"vendas";

  constructor(private http:HttpClient) { }

  salvarCategoria(nome){
    return this.http.post(this.url+"?token="+localStorage.getItem('token'),{
      nome:nome
    });
  }
  getCategorias(token,page,select):Observable<any[]>{
    let params = new HttpParams().set('token', token);
    params = params.set('page',page);
    params = params.set('combo',select);
    return this.http.get<any[]>(this.url,{params:params})
  }
  
  excluirCategorias(id , token){
    let params = new HttpParams().set('token', token);
    params = params.set('id',id);
    return this.http.delete(this.url+"/"+id,{
      params:params
    });
  }

  checkNomeCategoria(nome: string, id) {
    return this.http
      .get(this.urlVerificarNomeCategoria+"?token="+localStorage.getItem('token')+"&nome="+nome+"&id="+id)
      .pipe(map(data => data))
  }

}
