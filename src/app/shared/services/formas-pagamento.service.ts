import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class FormasPagamentoService {
  private url = environment.url+ "formasPagamento";
  private urlVerificarNomeFormasPagamento = environment.url+ "verificarFormasDePagamento";

 
  //private url_excluir = environment.url+"vendas";

  constructor(private http:HttpClient) { }

  salvarFormasDePagamento(nome){
    return this.http.post(this.url+"?token="+localStorage.getItem('token'),{
      nome:nome
    });
  }
  getFormasDePagamento(token,page,select):Observable<any[]>{
    let params = new HttpParams().set('token', token);
    params = params.set('page',page);
    params = params.set('combo',select);
    return this.http.get<any[]>(this.url,{params:params})
  }
  
  excluirFormasDePagamento(id , token){
    let params = new HttpParams().set('token', token);
    params = params.set('id',id);
    return this.http.delete(this.url+"/"+id,{
      params:params
    });
  }

  checkNomeFormasDePagamento(nome: string, id) {
    return this.http
      .get(this.urlVerificarNomeFormasPagamento+"?token="+localStorage.getItem('token')+"&nome="+nome+"&id="+id)
      .pipe(map(data => data))
  }

}
