import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import {ICliente} from '../interface/ICliente';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClientesService {
  private url = environment.url+"clientesErp";

  constructor(private http:HttpClient) { }

  getClientesHttp(token):Observable<ICliente[]>{
    let params = new HttpParams().set('token', token);
    return this.http.get<ICliente[]>(this.url,{params:params})
  }
  errosHttp(error:HttpErrorResponse){
    return Observable.throw(error.message || "Server Error")
  }

}
