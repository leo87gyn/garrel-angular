import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmpresasService {
  private url = environment.url+ "empresas";
 
  //private url_excluir = environment.url+"vendas";

  constructor(private http:HttpClient) { }

  salvarEmpresa(formulario , token){
    return this.http.post(this.url+"?token="+token,{
      formulario
    });
  }
  editarEmpresa(formulario , token){
    return this.http.patch(this.url+"/"+formulario['id']+"?token="+token,{
      formulario
    });
  }
  getEmpresas(token,page):Observable<any[]>{
    let params = new HttpParams().set('token', token);
    params = params.set('page',page);
    return this.http.get<any[]>(this.url,{params:params})
  }
  getEmpresa(id,token):Observable<any[]>{
    let params = new HttpParams().set('token', token);
    params = params.set('id',id);
    return this.http.get<any[]>(this.url+"/"+id,{params:params})
  }
  
  excluirEmpresa(id , token){
    let params = new HttpParams().set('token', token);
    params = params.set('id',id);
    return this.http.delete(this.url+"/"+id,{
      params:params
    });
  }
  /*
  enviarEmail(email , venda_id , token){
    return this.http.post(this.url_email+"?token="+token,{
      venda_id:venda_id,
      email:email
    });
  }
  
  getPdf(venda_id,token){
    let params = new HttpParams().set('token', token);
  
    return this.http.get(this.url_pdf+"?venda_id="+venda_id,{
      params:params
    });
  }
  getVendas(token,page):Observable<any[]>{
    let params = new HttpParams().set('token', token);
    params = params.set('page',page);
    return this.http.get<any[]>(this.url_vendas,{params:params})
  }
  errosHttp(error:HttpErrorResponse){
    return Observable.throw(error.message || "Server Error")
  }
  */

}
