import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import {IProdutos} from '../interface/IProduto';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProdutosService {
  private url =  environment.url+"produtosErp";
  private urlProdutos = environment.url+"produto";
  private urlVerificarNomeProduto = environment.url+"verificarProduto";

  constructor(private http:HttpClient) { }

  salvar(nome,preco,categoria_id){
    return this.http.post(this.urlProdutos+"?token="+localStorage.getItem('token'),{
      nome:nome,
      preco:preco,
      categoria_id:categoria_id
    });
  }

  getProdutosHttp(token):Observable<IProdutos[]>{
    let params = new HttpParams().set('token', token);
    return this.http.get<IProdutos[]>(this.url,{params:params})
  }

  getProdutos(token,page):Observable<any[]>{
    let params = new HttpParams().set('token', token);
    params = params.set('page',page);
    return this.http.get<any[]>(this.urlProdutos,{params:params})
  }
  
  errosHttp(error:HttpErrorResponse){
    return Observable.throw(error.message || "Server Error")
  }

  checkNomeProduto(nome: string, id) {
    return this.http
      .get(this.urlVerificarNomeProduto+"?token="+localStorage.getItem('token')+"&nome="+nome+"&id="+id)
      .pipe(map(data => data))
  }

}
