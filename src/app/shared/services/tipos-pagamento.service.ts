import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import {ICliente} from '../interface/ICliente';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TiposPagamentoService {
  private url = environment.url+"tiposDePagamento";

  constructor(private http:HttpClient) { }

  getTiposPagamentosHttp(pagina,token):Observable<ICliente[]>{
    let params = new HttpParams().set('token', token);
    params = params.set('pagina',pagina);
    return this.http.get<[any]>(this.url,{params:params})
  }
  errosHttp(error:HttpErrorResponse){
    return Observable.throw(error.message || "Server Error")
  }

}
