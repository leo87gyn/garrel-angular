import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class VendaService {
  private url = environment.url+ "venda";
  private url_vendas = environment.url+ "vendas";
  private url_pdf = environment.url+"gerarPdf";
  private url_email = environment.url+"enviarEmail";
  private url_excluir = environment.url+"vendas";

  constructor(private http:HttpClient) { }

  salvarVenda(json , token){
    return this.http.post(this.url+"?token="+token,{
      venda:json
    });
  }
  excluirVenda(venda_id , token){
    let params = new HttpParams().set('token', token);
    params = params.set('venda_id',venda_id);
    return this.http.delete(this.url_excluir+"/"+venda_id,{
      params:params
    });
  }
  enviarEmail(email , venda_id , token){
    return this.http.post(this.url_email+"?token="+token,{
      venda_id:venda_id,
      email:email
    });
  }
  getPdf(venda_id,token){
    let params = new HttpParams().set('token', token);
  
    return this.http.get(this.url_pdf+"?venda_id="+venda_id,{
      params:params
    });
  }
  getVendas(token,page):Observable<any[]>{
    let params = new HttpParams().set('token', token);
    params = params.set('page',page);
    return this.http.get<any[]>(this.url_vendas,{params:params})
  }
  errosHttp(error:HttpErrorResponse){
    return Observable.throw(error.message || "Server Error")
  }

}
