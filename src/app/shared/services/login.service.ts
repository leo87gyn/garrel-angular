import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private url = environment.url+"logar";
  constructor(private http:HttpClient) { }

  login(email,password){
    return this.http.post(this.url,{
      email:email,
      password:password
    });
  }
}
