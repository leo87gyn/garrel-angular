import { AbstractControl } from '@angular/forms';
import { CategoriasService } from '../services/categorias.service';
import { map } from 'rxjs/operators';

export class ValidateNomeEmpreendimento {
  static createValidator(categoriaService: CategoriasService,id) {
    return (control: AbstractControl) => {
      return categoriaService.checkNomeCategoria(control.value , id).pipe(
          map(res => {
        return res ? null : { nome: true };
      }));
    };
  }
}