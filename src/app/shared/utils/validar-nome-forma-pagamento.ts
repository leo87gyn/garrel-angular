import { AbstractControl } from '@angular/forms';
import { CategoriasService } from '../services/categorias.service';
import { map } from 'rxjs/operators';
import { FormasPagamentoService } from '../services/formas-pagamento.service';

export class ValidateNomeFormaPagamento {
  static createValidator(formaPagamentoService: FormasPagamentoService,id) {
    return (control: AbstractControl) => {
      return formaPagamentoService.checkNomeFormasDePagamento(control.value , id).pipe(
          map(res => {
        return res ? null : { nome: true };
      }));
    };
  }
}