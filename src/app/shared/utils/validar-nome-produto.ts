import { AbstractControl } from '@angular/forms';
import { map } from 'rxjs/operators';
import { ProdutosService } from '../services/produtos.service';

export class ValidateNomeProduto {
  static createValidator(produtoService: ProdutosService,id) {
    return (control: AbstractControl) => {
      return produtoService.checkNomeProduto(control.value , id).pipe(
          map(res => {
        return res ? null : { nome: true };
      }));
    };
  }
}