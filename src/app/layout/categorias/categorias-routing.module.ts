import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoriasListarComponent } from './categorias-listar.component';

const routes: Routes = [
    
    {
        path: '',
        component: CategoriasListarComponent,
    }
];

  
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CategoriasRoutinglModule {
}
