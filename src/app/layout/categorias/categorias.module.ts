import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from './../../shared';
import { ProdutosService } from './../../shared/services/produtos.service';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { NgbModule, NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { CategoriasListarComponent } from './categorias-listar.component';
import { CategoriasRoutinglModule } from './categorias-routing.module';
 

@NgModule({
    imports: [
        CommonModule, 
        CategoriasRoutinglModule,
        PageHeaderModule , 
        FormsModule , 
        NgSelectModule , 
        CurrencyMaskModule,
        
        NgbModule.forRoot()
    ],
    declarations: [CategoriasListarComponent],
    providers: [
        ProdutosService,
        
    ],
})
export class CategoriasModule {}
