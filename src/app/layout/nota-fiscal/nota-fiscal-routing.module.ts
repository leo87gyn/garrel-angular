import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotaFiscalListarComponent } from './nota-fiscal-listar.component';

const routes: Routes = [
    
    {
        path: '',
        component: NotaFiscalListarComponent,
    }
];

  
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class NotaFiscalRoutinglModule {
}
