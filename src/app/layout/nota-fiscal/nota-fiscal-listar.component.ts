import { Component, OnInit, OnDestroy } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { IProdutos } from '../../shared/interface/IProduto';
import { Router } from '@angular/router';
import { VendaService } from '../../shared/services/venda.service';
import { environment } from '../../../environments/environment';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-nota-fiscal-listar',
    templateUrl: './nota-fiscal-listar.component.html',
    styleUrls: ['./nota-fiscal-listar.component.scss'],
    animations: [routerTransition()]
})

export class NotaFiscalListarComponent implements OnInit, OnDestroy {
    vendas;
    myRadio;
    active = 1;//pagina atual
    modal;

    pages = [];

    produtosHttp: IProdutos[];
    constructor(
        private _vendaService: VendaService,
        private router: Router,
        private modalService: NgbModal,

    ) {
    }
    closeResult: string;

    open(content) {
        //console.log(content._def.references) nome do content
        if (this.myRadio == null) {
            alert("Escolha uma venda");
            return;
        }

        this.modal = this.modalService.open(content);
        this.modal.result.then((result) => {
            this.closeResult = `Closed with: ${result}`;

            console.log(result)
        }, (reason) => {
            console.log(reason)
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    gerarPdfEmpresa() {
        if (this.myRadio == null) {
            alert("Escolha um pdf")
        }
        else {
            var newWindow1Via = window.open(environment.url + 'gerarPdf?token=' + localStorage.getItem('token') + "&venda_id=" + this.myRadio + "&via=1");
            return newWindow1Via;
        }
    }

    gerarPdfCliente() {
        if (this.myRadio == null) {
            alert("Escolha um pdf")
        }
        else {
            var newWindow2Via = window.open(environment.url + 'gerarPdf?token=' + localStorage.getItem('token') + "&venda_id=" + this.myRadio + "&via=2");
            return newWindow2Via;
        }
    }
    paginou(e) {
        this.myRadio=null;
        this.active = e;
        this.listarVendas();
    }
    excluirVenda() {
        //this.myRadio;
        this._vendaService.excluirVenda(this.myRadio, localStorage.getItem('token'))
            .subscribe(retorno => {
                this.active=1;
                this.myRadio=null;
                this.listarVendas();
                this.modal.close()
            },
                error => {
                    //this.error = error,
                    this.modal.close(),
                        //this.isValid = false
                        console.log(error)
                }
            )

    }
    enviarEmail(email) {

        const validar = this.validarEmail(email);
        if (validar) {
            this._vendaService.enviarEmail(email, this.myRadio, localStorage.getItem('token'))
                .subscribe(retorno => {
                    console.log(retorno)
                    this.modal.close()
                },
                    error => {
                        //this.error = error,
                        this.modal.close(),
                            //this.isValid = false
                            console.log(error)
                    }
                )
        }
        else {
            alert("Digite um e-mail válido")
        }
    }
    validarEmail(email) {
        if (email == null || email == "") {
            return false;
        }
        else {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());//true se email valido

        }

    }

    listarVendas() {
        this.vendas=[];
        this.pages=[];
        this._vendaService.getVendas(localStorage.getItem('token') , this.active)
            .subscribe(
                (res: any) => {
                    this.vendas = res.data;
                    for (let index = 0; index < res.last_page; index++) {
                        
                        this.pages.push(index)
                    }
                },
                error => {
                    console.log(error),
                        this.router.navigate(['/login'])
                });
    }

    

    ngOnInit() {
        this.listarVendas();
    }
    ngOnDestroy() {

    }


}
