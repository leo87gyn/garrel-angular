import { Component, OnInit, OnDestroy} from '@angular/core';
import { routerTransition } from '../../router.animations';

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss'],
    animations: [routerTransition()]
})

export class FormComponent implements OnInit , OnDestroy {
    fieldArray: Array<any> = [];
    newAttribute: any = {};
    validaFormulario:boolean;

    addFieldValue() {
        this.validaFormulario = this.validarFormulario();
        if(this.validaFormulario){
            this.fieldArray.push(this.newAttribute)
        }
    }

    deleteFieldValue(index) {
        this.fieldArray.splice(index, 1);
    }

    validarFormulario(){
        if(!this.newAttribute.hasOwnProperty("carne") || this.newAttribute.carne==null ){
            return false;
        }
        if(!this.newAttribute.hasOwnProperty("quantidade") || this.newAttribute.quantidade==null ){
            return false;
        }
        if(!this.newAttribute.hasOwnProperty("preco") || this.newAttribute.preco==null ){
            return false;
        }
        return true
    }

    produtos=[{}];
    text=[];
    total;
    simpleItems = [
        {
            name:"picanha",
            id:1,
            preco:"20"
        },
        {
            name:"alcatra",
            id:2,
            preco:"10"

        },
        {
            name:"fraldinha",
            id:3,
            preco:"5"

        }
    ];
    constructor() {    
    }

    ngOnInit() {}
    ngOnDestroy(){
        //alert("produtos_id "+this.selectedSimpleItem);
        //console.log("precos "+this.value);
        console.log(this.fieldArray)
        //console.log(this.vc)
    }
    
}
