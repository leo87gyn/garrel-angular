import { Component, OnInit, OnDestroy } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { IProdutos } from '../../shared/interface/IProduto';
import { Router,ActivatedRoute } from '@angular/router';
import { EmpresasService } from '../../shared/services/empresas.service';



@Component({
    selector: 'app-empresas-cadastrar',
    templateUrl: './empresas-cadastrar.component.html',
    styleUrls: ['./empresas-cadastrar.component.scss'],
    animations: [routerTransition()]
})

export class EmpresasCadastrarComponent implements OnInit, OnDestroy {
    razao_social;
    fantasia;
    proprietario;
    email;
    endereco;
    telefone;
    cidade;
    cep;
    formulario;
    isValid = false;
    empresa$;
    operacao;
    editar;

    validarFormulario() {
        if (this.razao_social == null ) {
            alert("Razão social é obrigatoório");
            return false;
        }
        if(this.fantasia == null){this.fantasia=""}
        if(this.proprietario == null){this.proprietario=""}
        if(this.email == null){this.email=""}
        if(this.endereco == null){this.endereco=""}
        if(this.telefone == null){this.telefone=""}
        if(this.cidade == null){this.cidade=""}
        if(this.cep == null){this.cep=""}

        this.formulario={
            'id':this.empresa$,
            'razao_social':this.razao_social,
            'fantasia':this.fantasia,
            'proprietario':this.proprietario,
            'email':this.email,
            'endereco':this.endereco,
            'telefone':this.telefone,
            'cidade':this.cidade,
            'cep':this.cep
        }
        return true;
    }
    

    finalizar() {
        var validar = this.validarFormulario();
           if(validar && this.editar==false){
            this._empresasService.salvarEmpresa(this.formulario, localStorage.getItem('token'))
                .subscribe(retorno => {
                    console.log(retorno)
                    //this.isValid = false;
                    if(retorno['success']==true){
                        alert(retorno['mensagem']);
                        this.router.navigate(['/empresas']);
                    }
                    else{
                        
                        alert(retorno['mensagem']);
                    }
                    //
                },
                    error => {
                        this.error = error,
                        this.isValid=false,
                        this.router.navigate(['/login']);
                    }

                    //this.router.navigate['/login']
                )
    

        
           }
           else{
               console.log("Enviar Edicao");
               console.log(this.formulario);
               this._empresasService.editarEmpresa(this.formulario, localStorage.getItem('token'))
               .subscribe(retorno => {
                   console.log(retorno)
                   //this.isValid = false;
                   if(retorno['success']==true){
                       //alert(retorno['mensagem']);
                       this.router.navigate(['/empresas']);
                   }
                   else{
                       
                       alert(retorno['mensagem']);
                   }
                   //
               },
                   error => {
                       this.error = error,
                       this.isValid=false,
                       this.router.navigate(['/login']);
                   }

                   //this.router.navigate['/login']
               )


           }
               

    }

    simpleItems = [];
    clientes = [];
    produtosHttp: IProdutos[];
    pagamentos = [];
    error;
  
    constructor(
        
        private _empresasService: EmpresasService,
        private router: Router,
        private route:ActivatedRoute
        /*
        private _clientesService: ClientesService,
        private _vendaService: VendaService,
        private _tiposPagamentoService: TiposPagamentoService,
        private router: Router
        */

    ) {
        this.route.params.subscribe( params => this.empresa$ = params.id)
    }

    ngOnInit() {
        //inserir
        if(this.empresa$ === "nova-empresa"){
            
            this.operacao="Salvar Empresa";
            this.editar=false;
        }
        //editar
        else{
            this.operacao="Editar Empresa";
            this.editar=true;
            this._empresasService.getEmpresa(this.empresa$, localStorage.getItem('token'))
                .subscribe(retorno => {
                    //console.log(retorno)
                    //this.isValid = false;
                    if(retorno['success']==true){
                        this.razao_social = retorno['empresa']['razao_social'];
                        this.fantasia = retorno['empresa']['fantasia'];
                        this.proprietario = retorno['empresa']['proprietario'];
                        this.email = retorno['empresa']['email'];
                        this.endereco = retorno['empresa']['endereco'];
                        this.telefone = retorno['empresa']['telefone'];
                        this.cidade = retorno['empresa']['cidade'];
                        this.cep =retorno['empresa']['cep'];
                    }
                    else{
                        
                        alert(retorno['mensagem']);
                    }
                    
                },
                    error => {
                        console.log(error);
                        
                        this.router.navigate(['/login']);
                    }

                    //this.router.navigate['/login']
                )
    
        }
    }
    ngOnDestroy() {
       
    }

}
