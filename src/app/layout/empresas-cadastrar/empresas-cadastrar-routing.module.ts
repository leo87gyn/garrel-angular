import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmpresasCadastrarComponent } from './empresas-cadastrar.component';

const routes: Routes = [
    {
        path: '',
        component: EmpresasCadastrarComponent,
    }
]
  
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EmpresasCadastrarRoutinglModule {
}
