import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from './../../shared';
import { ProdutosService } from './../../shared/services/produtos.service';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { CURRENCY_MASK_CONFIG } from "ng2-currency-mask/src/currency-mask.config";
import { ClientesService } from '../../shared/services/clientes.service';
import { VendaService } from '../../shared/services/venda.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EmpresasCadastrarComponent } from './empresas-cadastrar.component';
import { EmpresasCadastrarRoutinglModule } from './empresas-cadastrar-routing.module';
@NgModule({
    imports: [
        CommonModule,
        EmpresasCadastrarRoutinglModule,
        PageHeaderModule,
        FormsModule,
        NgSelectModule,
        CurrencyMaskModule,
        NgbModule.forRoot()
    ],
    declarations: [EmpresasCadastrarComponent],
    providers: [
        ProdutosService,
        ClientesService,
        //NgbModalRef,
        VendaService
    ],
})
export class EmpresasCadastrarModule {
}