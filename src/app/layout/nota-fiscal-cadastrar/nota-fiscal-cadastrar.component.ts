import { Component, OnInit, OnDestroy } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { ProdutosService } from '../../shared/services/produtos.service';
import { IProdutos } from '../../shared/interface/IProduto';
import { ClientesService } from '../../shared/services/clientes.service';
import { Router } from '@angular/router';
import { VendaService } from '../../shared/services/venda.service';
import { TiposPagamentoService } from '../../shared/services/tipos-pagamento.service';

@Component({
    selector: 'app-nota-fiscal-cadastrar',
    templateUrl: './nota-fiscal-cadastrar.component.html',
    styleUrls: ['./nota-fiscal-cadastrar.component.scss'],
    animations: [routerTransition()]
})

export class NotaFiscalCadastrarComponent implements OnInit, OnDestroy {
    fieldArray: Array<any> = [];
    newAttribute: any = {};
    validaFormulario: boolean;
    total = 0;
    cliente;
    pagamento_model;
    pagamento;
    isValid = false;

    addFieldValue() {
        this.validaFormulario = this.validarFormulario();
        if (this.validaFormulario) {

            var quantidade = this.newAttribute.quantidade;
            var preco = this.newAttribute.preco;
            var total = quantidade * preco;

            this.newAttribute["total"] = total;
            this.fieldArray.push(this.newAttribute)

            this.newAttribute = {};
            this.calcularTotal();

        }
    }

    deleteFieldValue(index) {
        this.fieldArray.splice(index, 1);
        this.calcularTotal();
    }

    validarFormulario() {
        if (!this.newAttribute.hasOwnProperty("carne") || this.newAttribute.carne == null) {
            alert("Escolha o produto !");
            return false;
        }
        if (!this.newAttribute.hasOwnProperty("quantidade") || this.newAttribute.quantidade == null || this.newAttribute.quantidade == 0) {
            alert("Quantidade é obrigatório !");
            return false;
        }
        if (!this.newAttribute.hasOwnProperty("preco") || this.newAttribute.preco == null || this.newAttribute.preco == 0) {
            alert("Preço é obrigatório !");
            return false;
        }
        return true;
    }
    calcularTotal() {
        var totalGeral = 0;
        for (let index = 0; index < this.fieldArray.length; index++) {
            console.log(parseFloat(this.fieldArray[index].total))
            totalGeral += parseFloat(this.fieldArray[index].total);
            //parseInt(totalGeral) += parseInt( this.fieldArray[index].total );
        }
        this.total = totalGeral;
        console.log(totalGeral)

    }
    editarPreco(indice, preco) {
        if (preco == null || preco == 0) {
            this.isValid = true;
            var qnt = this.fieldArray[indice]["quantidade"];
            this.fieldArray[indice]["preco"] = preco;
            var total = qnt * preco;
            this.fieldArray[indice]["total"] = total;
            this.calcularTotal();

        }
        else {
            var qnt = this.fieldArray[indice]["quantidade"];
            this.fieldArray[indice]["preco"] = preco;
            var total = qnt * preco;
            this.fieldArray[indice]["total"] = total;
            this.isValid = false;
            this.calcularTotal();
        }

    }
    editarQuantidade(indice, quantidade) {
        if (quantidade == 0 || quantidade == null) {
            this.isValid = true;
            var preco = this.fieldArray[indice]["preco"];
            this.fieldArray[indice]["quantidade"] = quantidade;
            var total = quantidade * preco;
            this.fieldArray[indice]["total"] = total;
            this.calcularTotal();
        }
        else {
            this.isValid = false;
            var preco = this.fieldArray[indice]["preco"];
            this.fieldArray[indice]["quantidade"] = quantidade;
            var total = quantidade * preco;
            this.fieldArray[indice]["total"] = total;
            this.calcularTotal();
        }

    }
    editarCarne(carne_id, i) {
        if (carne_id == null) {//se nao houver carne limpar valor
            this.fieldArray[i]['preco'] = 0;
            this.fieldArray[i]['total'] = 0;
            this.calcularTotal();
            this.isValid = true;
        }
        else {
            for (let index = 0; index < this.simpleItems.length; index++) {
                if (this.simpleItems[index].codigo_produto == carne_id) {
                    //this.newAttribute.preco = this.simpleItems[index].valor_unitario
                    this.fieldArray[i]["preco"] = this.simpleItems[index].valor_unitario;
                    var preco = this.fieldArray[i]["preco"];
                    var quantidade = this.fieldArray[i]["quantidade"];
                    this.fieldArray[i]["total"] = preco * quantidade;
                }

            }
            this.calcularTotal();
            this.isValid = false;
        }
    }
    alterarCarne(e) {
        for (let index = 0; index < this.simpleItems.length; index++) {
            if (this.simpleItems[index].codigo_produto == e) {
                this.newAttribute.preco = this.simpleItems[index].valor_unitario
            }

        }
    }

    finalizar() {
        if (this.cliente == null || this.pagamento_model==null) {
            alert("Cliente e tipo de pagamento é obrigatório !");
            
        }
        else {
            //console.log(this.pagamento);
            console.log(this.pagamento_model);
           
           
            this.isValid = true;
            this.fieldArray[0]['cliente'] = this.cliente;
            this.fieldArray[0]['pagamento_id'] = this.pagamento_model.cCodigo;
            this.fieldArray[0]['pagamento_nome'] = this.pagamento_model.cDescricao;
            this._vendaService.salvarVenda(this.fieldArray, localStorage.getItem('token'))
                .subscribe(retorno => {
                    console.log(retorno),
                        this.isValid = false;
                    this.router.navigate(['/nota-fiscal'])
                },
                    error => {
                        this.error = error,
                        this.isValid=false
                    }

                    //this.router.navigate['/login']
                )

        }



    }

    simpleItems = [];
    clientes = [];
    produtosHttp: IProdutos[];
    pagamentos = [];
    error;
    constructor(
        private _produtosService: ProdutosService,
        private _clientesService: ClientesService,
        private _vendaService: VendaService,
        private _tiposPagamentoService: TiposPagamentoService,
        private router: Router

    ) {
    }

    ngOnInit() {
        //alert(environment.production);
        this._produtosService.getProdutosHttp(localStorage.getItem('token'))
            .subscribe(data => this.simpleItems = data['produto_servico_resumido'],
                err => {
                    console.log(err),
                        this.router.navigate(['/login'])
                }
            );
        this._clientesService.getClientesHttp(localStorage.getItem('token'))
            .subscribe(data => this.clientes = data['clientes_cadastro_resumido'],
                err => {
                    console.log(err),
                        this.router.navigate(['/login'])
                }
            );
        this._tiposPagamentoService.getTiposPagamentosHttp(1,localStorage.getItem('token'))
        .subscribe(data => this.pagamentos = data['cadastros'],
            err => {
                console.log(err),
                    this.router.navigate(['/login'])
            }
        );
    }
    ngOnDestroy() {

        console.log(this.fieldArray)
    }

}
