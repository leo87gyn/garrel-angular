import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from './../../shared';
import { ProdutosService } from './../../shared/services/produtos.service';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { CurrencyMaskConfig, CURRENCY_MASK_CONFIG } from "ng2-currency-mask/src/currency-mask.config";
import { ClientesService } from '../../shared/services/clientes.service';
import { VendaService } from '../../shared/services/venda.service';
import { NgbModule, NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NotaFiscalCadastrarComponent } from './nota-fiscal-cadastrar.component';
import { NotaFiscalCadastrarRoutinglModule } from './nota-fiscal-cadastrar-routing.module';


export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
    align: "center",
    allowNegative: true,
    decimal: ",",
    precision: 2,
    prefix: "R$ ",
    suffix: "",
    thousands: "."
};

@NgModule({
    imports: [
        CommonModule, 
        NotaFiscalCadastrarRoutinglModule, 
        PageHeaderModule , 
        FormsModule , 
        NgSelectModule , 
        CurrencyMaskModule,
        
        NgbModule.forRoot()
    ],
    declarations: [NotaFiscalCadastrarComponent],
    providers: [
        ProdutosService,
        ClientesService,
        //NgbModalRef,
        VendaService,
        { provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig }
    ],
})
export class NotaFiscalCadastrarModule {}
