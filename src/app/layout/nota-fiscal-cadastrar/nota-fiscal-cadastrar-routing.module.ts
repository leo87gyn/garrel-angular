import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotaFiscalCadastrarComponent } from './nota-fiscal-cadastrar.component';

const routes: Routes = [
    {
        path: '',
        component: NotaFiscalCadastrarComponent,
    }
]
  
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class NotaFiscalCadastrarRoutinglModule {
}
