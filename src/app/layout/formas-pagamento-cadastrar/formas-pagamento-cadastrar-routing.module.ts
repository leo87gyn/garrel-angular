import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormasPagamentoCadastrarComponent } from './formas-pagamento-cadastrar.component';

const routes: Routes = [
    
    {
        path: '',
        component: FormasPagamentoCadastrarComponent,
    }
];

  
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FormasPagamentoCadastrarRoutinglModule {
}
