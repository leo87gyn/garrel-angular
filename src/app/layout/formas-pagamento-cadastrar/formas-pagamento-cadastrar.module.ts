import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from './../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CategoriasService } from '../../shared/services/categorias.service';
import { FormasPagamentoCadastrarComponent } from './formas-pagamento-cadastrar.component';
import { FormasPagamentoCadastrarRoutinglModule } from './formas-pagamento-cadastrar-routing.module';

@NgModule({
    imports: [
        CommonModule, 
        FormasPagamentoCadastrarRoutinglModule,
        PageHeaderModule , 
        FormsModule , 
        NgSelectModule , 
        FormsModule, 
        ReactiveFormsModule,
        CurrencyMaskModule,
        NgbModule.forRoot()
    ],
    declarations: [FormasPagamentoCadastrarComponent],
    providers: [
        CategoriasService,
        
    ],
})
export class FormasPagamentoCadastrarModule {}
