import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoriasCadastrarComponent } from './categorias-cadastrar.component';

const routes: Routes = [
    
    {
        path: '',
        component: CategoriasCadastrarComponent,
    }
];

  
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CategoriasCadastrarRoutinglModule {
}
