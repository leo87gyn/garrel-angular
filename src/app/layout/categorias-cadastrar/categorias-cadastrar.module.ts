import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from './../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CategoriasCadastrarComponent } from './categorias-cadastrar.component';
import { CategoriasCadastrarRoutinglModule } from './categorias-cadastrar-routing.module';
import { CategoriasService } from '../../shared/services/categorias.service';

@NgModule({
    imports: [
        CommonModule, 
        CategoriasCadastrarRoutinglModule,
        PageHeaderModule , 
        FormsModule , 
        NgSelectModule , 
        FormsModule, 
        ReactiveFormsModule,
        CurrencyMaskModule,
        NgbModule.forRoot()
    ],
    declarations: [CategoriasCadastrarComponent],
    providers: [
        CategoriasService,
        
    ],
})
export class CategoriasCadastrarModule {}
