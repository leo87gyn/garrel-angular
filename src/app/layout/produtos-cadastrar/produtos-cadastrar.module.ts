import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from './../../shared';
import { ProdutosService } from './../../shared/services/produtos.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { CURRENCY_MASK_CONFIG, CurrencyMaskConfig } from "ng2-currency-mask/src/currency-mask.config";
import { ClientesService } from '../../shared/services/clientes.service';
import { VendaService } from '../../shared/services/venda.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProdutosCadastrarRoutinglModule } from './produtos-cadastrar-routing.module';
import { ProdutosCadastrarComponent } from './produtos-cadastrar.component';
import { CategoriasService } from '../../shared/services/categorias.service';

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
    align: "center",
    allowNegative: true,
    decimal: ",",
    precision: 2,
    prefix: "R$ ",
    suffix: "",
    thousands: "."
};

@NgModule({
    imports: [
        CommonModule,
        ProdutosCadastrarRoutinglModule,
        PageHeaderModule,
        FormsModule,
        NgSelectModule,
        CurrencyMaskModule,
        FormsModule, 
        ReactiveFormsModule,
        NgbModule.forRoot()
    ],
    declarations: [ProdutosCadastrarComponent],
    providers: [
        ProdutosService,
        ClientesService,
        CategoriasService,
        VendaService,
        { provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig }
    ],
})
export class ProdutosCadastrarModule {
}