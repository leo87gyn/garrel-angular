import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmpresasListarComponent } from './empresas-listar.component';

const routes: Routes = [
    
    {
        path: '',
        component: EmpresasListarComponent,
    }
];

  
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EmpresasRoutinglModule {
}
