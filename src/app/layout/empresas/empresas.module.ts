import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmpresasRoutinglModule } from './empresas-routing.module';
import { PageHeaderModule } from './../../shared';
import { ProdutosService } from './../../shared/services/produtos.service';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { CurrencyMaskConfig, CURRENCY_MASK_CONFIG } from "ng2-currency-mask/src/currency-mask.config";
import { ClientesService } from '../../shared/services/clientes.service';
import { VendaService } from '../../shared/services/venda.service';
import { NgbModule, NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EmpresasListarComponent } from './empresas-listar.component';
 

@NgModule({
    imports: [
        CommonModule, 
        EmpresasRoutinglModule, 
        PageHeaderModule , 
        FormsModule , 
        NgSelectModule , 
        CurrencyMaskModule,
        
        NgbModule.forRoot()
    ],
    declarations: [EmpresasListarComponent],
    providers: [
        ProdutosService,
        ClientesService,
        //NgbModalRef,
        VendaService
    ],
})
export class EmpresasModule {}
