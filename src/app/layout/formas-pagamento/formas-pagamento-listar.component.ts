import { Component, OnInit, OnDestroy } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { IProdutos } from '../../shared/interface/IProduto';
import { Router } from '@angular/router';
import { VendaService } from '../../shared/services/venda.service';
import { environment } from '../../../environments/environment';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EmpresasService } from '../../shared/services/empresas.service';
import { CategoriasService } from '../../shared/services/categorias.service';
import { FormasPagamentoService } from '../../shared/services/formas-pagamento.service';

@Component({
    selector: 'app-formas-pagamento-listar',
    templateUrl: './formas-pagamento-listar.component.html',
    styleUrls: ['./formas-pagamento-listar.component.scss'],
    animations: [routerTransition()]
})

export class FormasPagamentoListarComponent implements OnInit, OnDestroy {
    formasDePagamento;
    myRadio;
    active = 1;//pagina atual
    modal;

    pages = [];

    produtosHttp: IProdutos[];
    constructor(
        private _formasPagamentoService: FormasPagamentoService,
        private router: Router,
        private modalService: NgbModal,

    ) {
    }
    closeResult: string;

    open(content) {
        //console.log(content._def.references) nome do content
        if (this.myRadio == null) {
            alert("Escolha uma empresa");
            return;
        }

        this.modal = this.modalService.open(content);
        this.modal.result.then((result) => {
            this.closeResult = `Closed with: ${result}`;

            console.log(result)
        }, (reason) => {
            console.log(reason)
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    editar(){
        if (this.myRadio == null) {
            alert("Escolha uma empresa");
            return;
        }
        else{
            this.router.navigate(['/empresas-cadastrar/'+this.myRadio])
        }
    }

    
    paginou(e) {
        this.myRadio=null;
        this.active = e;
        this.listarFormasDePamento();
    }
    
    excluirEmpresa() {
        this.myRadio;
        
        this._formasPagamentoService.excluirFormasDePagamento(this.myRadio, localStorage.getItem('token'))
            .subscribe(retorno => {
                this.active=1;
                this.myRadio=null;
                this.listarFormasDePamento();
                this.modal.close()
            },
                error => {
                    //this.error = error,
                    this.modal.close(),
                        //this.isValid = false
                        console.log(error)
                }
            )
            

    }
    
    

    listarFormasDePamento() {
        this.formasDePagamento=[];
        this.pages=[];
        this._formasPagamentoService.getFormasDePagamento(localStorage.getItem('token') , this.active , null)
            .subscribe(
                (res: any) => {
                    this.formasDePagamento = res.data;
                    for (let index = 0; index < res.last_page; index++) {
                        
                        this.pages.push(index)
                    }
                },
                error => {
                    console.log(error),
                        this.router.navigate(['/login'])
                });
    }

    

    ngOnInit() {
        this.listarFormasDePamento();
    }
    ngOnDestroy() {

    }


}
