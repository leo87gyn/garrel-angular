import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormasPagamentoListarComponent } from './formas-pagamento-listar.component';

const routes: Routes = [
    
    {
        path: '',
        component: FormasPagamentoListarComponent,
    }
];

  
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FormasPagamentoRoutinglModule {
}
