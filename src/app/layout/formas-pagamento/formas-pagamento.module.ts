import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from './../../shared';
import { ProdutosService } from './../../shared/services/produtos.service';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { NgbModule, NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormasPagamentoRoutinglModule } from './formas-pagamento-routing.module';
import { FormasPagamentoListarComponent } from './formas-pagamento-listar.component';
import { FormasPagamentoService } from '../../shared/services/formas-pagamento.service';


@NgModule({
    imports: [
        CommonModule, 
        FormasPagamentoRoutinglModule,
        PageHeaderModule , 
        FormsModule , 
        NgSelectModule , 
        CurrencyMaskModule,
        
        NgbModule.forRoot()
    ],
    declarations: [FormasPagamentoListarComponent],
    providers: [
        FormasPagamentoService,
        
    ],
})
export class FormasPagamentoModule {}
