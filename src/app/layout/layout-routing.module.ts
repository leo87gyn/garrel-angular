import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
            { path: 'forms', loadChildren: './form/form.module#FormModule' },
            { path: 'bs-element', loadChildren: './bs-element/bs-element.module#BsElementModule' },
            { path: 'grid', loadChildren: './grid/grid.module#GridModule' },
            { path: 'components', loadChildren: './bs-component/bs-component.module#BsComponentModule' },
            { path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' },
            { path: 'nota-fiscal', loadChildren: './nota-fiscal/nota-fiscal.module#NotaFiscalModule' },
            { path: 'nota-fiscal-cadastrar', loadChildren: './nota-fiscal-cadastrar/nota-fiscal-cadastrar.module#NotaFiscalCadastrarModule' },
            { path: 'empresas', loadChildren: './empresas/empresas.module#EmpresasModule' },
            { path: 'empresas-cadastrar/:id', loadChildren: './empresas-cadastrar/empresas-cadastrar.module#EmpresasCadastrarModule' },
            { path: 'produtos', loadChildren: './produtos/produtos.module#ProdutosModule' },
            { path: 'produtos-cadastrar', loadChildren: './produtos-cadastrar/produtos-cadastrar.module#ProdutosCadastrarModule' },
            { path: 'categorias', loadChildren: './categorias/categorias.module#CategoriasModule' },
            { path: 'categorias-cadastrar', loadChildren: './categorias-cadastrar/categorias-cadastrar.module#CategoriasCadastrarModule' },
            { path: 'formas-pagamento', loadChildren: './formas-pagamento/formas-pagamento.module#FormasPagamentoModule' },
            { path: 'formas-pagamento-cadastrar', loadChildren: './formas-pagamento-cadastrar/formas-pagamento-cadastrar.module#FormasPagamentoCadastrarModule' }
            
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
