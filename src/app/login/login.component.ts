import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { LoginService } from '../shared/services/login.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    email;
    password;
    constructor(
        public router: Router,
        private loginService:LoginService,
    ) {}

    ngOnInit() {
        
    }

    onLoggedin() {
        this.loginService.login(this.email,this.password)
                .subscribe(retorno => {
                    console.log(retorno)
                    localStorage.setItem('isLoggedin', 'true');
                    localStorage.setItem('token', retorno['token']);
                    localStorage.setItem('nome_usuario' , retorno['usuario']['name']);
                    //console.log(localStorage.getItem('nome_usuario'));
                    this.router.navigate(['/dashboard'])
                    },
                    err=>{
                        console.log(err)
                        alert("Login ou senha inválidos !")
                    }
                )
        
        
    }
}
